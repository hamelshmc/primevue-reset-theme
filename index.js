const exportCSS = () => {
  const rules = document.styleSheets[0].rules;
  const sizeCSS = document.styleSheets[0].rules.length;
  let cssOutputString = "";
  for (let index = 0; index < sizeCSS; index++) {
    cssOutputString = cssOutputString + rules[index].cssText + "\n\n";
  }
  cssOutputString = cssOutputString.replaceAll(
    "rgb(255, 255, 255)",
    "var(--surface-a)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(0, 0, 0, 0.87)",
    "var(--text-color)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "Roboto, Helvetica Neue Light, Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif",
    "var(--font-family)"
  );
  //* PRIMARY */
  cssOutputString = cssOutputString.replaceAll(
    "rgb(63, 81, 181)",
    "var(--primary)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(63, 81, 181, 0.68)",
    "var(--primary-active)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(63, 81, 181, 0.76)",
    "var(--primary-focus)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(63, 81, 181, 0.92)",
    "var(--primary-hover)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(63, 81, 181, 0.12)",
    "var(--primary-background-01)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(63, 81, 181, 0.5)",
    "var(--primary-background-02)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(63, 81, 181, 0.32)",
    "var(--primary-background-03)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(63, 81, 181, 0.04)",
    "var(--primary-background-04)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(63, 81, 181, 0.16)",
    "var(--primary-background-05)"
  );
  //* SECONDARY */
  cssOutputString = cssOutputString.replaceAll(
    "rgb(255, 64, 129)",
    "var(--secondary)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(255, 64, 129, 0.92)",
    "var(--secondary-active)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(255, 64, 129, 0.68)",
    "var(--secondary-focus)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(63, 81, 181, 0.76)",
    "var(--secondary-hover)"
  );
  //* SUCCESS */
  cssOutputString = cssOutputString.replaceAll(
    "rgb(104, 159, 56)",
    "var(--success)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(104, 159, 56, 0.92)",
    "var(--success-hover)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(104, 159, 56, 0.68)",
    "var(--success-focus)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(104, 159, 56, 0.76)",
    "var(--success-hover)"
  );
  //* INFO */
  cssOutputString = cssOutputString.replaceAll(
    "rgb(33, 150, 243)",
    "var(--info)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(33, 150, 243, 0.92)",
    "var(--info-hover)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(33, 150, 243, 0.68)",
    "var(--info-focus)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(33, 150, 243, 0.76)",
    "var(--info-hover)"
  );
  //* WARNING */
  cssOutputString = cssOutputString.replaceAll(
    "rgb(251, 192, 45)",
    "var(--warning)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(251, 192, 45, 0.92)",
    "var(--warning-hover)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(251, 192, 45, 0.68)",
    "var(--warning-focus)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(251, 192, 45, 0.76)",
    "var(--warning-hover)"
  );
  //* HELP */
  cssOutputString = cssOutputString.replaceAll(
    "rgb(156, 39, 176)",
    "var(--help)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(156, 39, 176, 0.92)",
    "var(--help-hover)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(156, 39, 176, 0.68)",
    "var(--help-focus)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(156, 39, 176, 0.76)",
    "var(--help-hover)"
  );
  //* DANGER */
  cssOutputString = cssOutputString.replaceAll(
    "rgb(211, 47, 47)",
    "var(--danger)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(211, 47, 47, 0.92)",
    "var(--danger-hover)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(211, 47, 47, 0.68)",
    "var(--danger-focus)"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(211, 47, 47, 0.76)",
    "var(--danger-hover)"
  );
  //* ERROR */
  cssOutputString = cssOutputString.replaceAll(
    "rgb(176, 0, 32)",
    "var(--error)"
  );
  // * BORDER RADIUS */
  cssOutputString = cssOutputString.replaceAll(
    "radius: 4px;",
    "radius: var(--border-radius);"
  );
  // * BOX-SHADOW */
  cssOutputString = cssOutputString.replaceAll(
    "rgba(0, 0, 0, 0.2) 0px 5px 5px -3px, rgba(0, 0, 0, 0.14) 0px 8px 10px 1px, rgba(0, 0, 0, 0.12) 0px 3px 14px 2px;",
    "var(--box-shadow);"
  );
  cssOutputString = cssOutputString.replaceAll(
    "rgba(0, 0, 0, 0.2) 0px 2px 1px -1px, rgba(0, 0, 0, 0.14) 0px 1px 1px 0px, rgba(0, 0, 0, 0.12) 0px 1px 3px 0px;",
    "var(--box-shadow);"
  );

  let blob = new Blob([cssOutputString]);
  saveAs(blob, "newTheme.css");
};

/** ejecutar método */
exportCSS();
